#!/usr/bin/env python

from shell_factory import (
    inspect_func,
    generate_command,
    generate_shell,
    Shell,
)
